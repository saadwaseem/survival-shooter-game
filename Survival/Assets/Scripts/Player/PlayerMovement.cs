﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
    {
       
        public float speed = 6f;

        Vector3 movement;
        Animator anim;
        Rigidbody playerRigidbody;
        int floorMask;
        float camRayLength = 100f;

        protected Joystick joystick;
        public VirtualJoyStick joystick2;

        void Awake()
       {
            joystick = FindObjectOfType<Joystick>();

            floorMask = LayerMask.GetMask("Floor");
            anim = GetComponent<Animator>();
            playerRigidbody = GetComponent<Rigidbody>();

        }


    void FixedUpdate()
    {

        //float h = Input.GetAxisRaw("Horizontal");
        //float v = Input.GetAxisRaw("Vertical");

        float h1 = joystick2.Horizontal();
        float v1 = joystick2.Vertical();

        Debug.Log(h1 + "  " + v1);


            Move(h1, v1);
            Turning();
            Animating(h1, v1);
        }

        void Move(float h, float v)
        {
            movement.Set(h, 0f, v);
            movement = movement.normalized * speed * Time.deltaTime;

            playerRigidbody.MovePosition(transform.position + movement);
        }

    void Turning()
    {

        float h = joystick.Horizontal;
        float v = joystick.Vertical;

        Vector3 turndir = new Vector3(h, 0, v);

        Vector3 playerToMouse = (transform.position + turndir) - transform.position;

        playerToMouse.y = 0f;


        Quaternion newRotation = Quaternion.LookRotation(playerToMouse);

        Debug.Log(h + "  " + v);
        playerRigidbody.MoveRotation(newRotation);

        }
        void Animating(float h, float v)
        {
            bool walking = h != 0f || v != 0f;
            anim.SetBool("IsWalking", walking);

        }
    }

